import { AppDataSource } from "./data-source"

const orderDto = {
    orderItem: [
        { productid: 1, qty: 1 },
        { productid: 2, qty: 2 },
        { productid: 4, qty: 1 },
    ],
    userId: 2
}

AppDataSource.initialize().then(async () => {

}).catch(error => console.log(error))
