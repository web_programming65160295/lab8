import { AppDataSource } from "./data-source"
import { Role } from "./entity/Role";
import { Type } from "./entity/Type";

AppDataSource.initialize().then(async () => {
    const typesRepository = AppDataSource.getRepository(Type);
    var type = new Type();
    type.id = 1;
    type.name = "drink";
    await typesRepository.save(type);

    type = new Type();
    type.id = 2;
    type.name = "bakery";
    await typesRepository.save(type);

    type = new Type();
    type.id = 3;
    type.name = "food";
    await typesRepository.save(type);

    const roles = await typesRepository.find({ order: { id: "asc" } })
    console.log(roles)
}).catch(error => console.log(error))
